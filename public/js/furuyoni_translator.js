"use strict";

const WORDS = [
	[["유리나", "유리나O"], ["刀", "ユリナ"]],
	[["유리나A1", "유리나A"], ["古", "古刀", "Aユリナ", "A1ユリナ"]],
	[["유리나A2"], ["心", "A2ユリナ"]],
	[["사이네", "사이네O"], ["薙", "薙刀", "サイネ"]],
	[["사이네A1", "사이네A"], ["琵", "琵琶", "Aサイネ", "A1サイネ"]],
	[["사이네A2"], ["拒", "拒絶", "A2サイネ", "徒神サイネ"]],
	[["히미카", "히미카O"], ["銃", "ヒミカ"]],
	[["히미카A"], ["炎", "Aヒミカ"]],
	[["토코요", "토코요O"], ["扇", "トコヨ"]],
	[["토코요A1", "토코요A"], ["笛", "Aトコヨ", "A1トコヨ"]],
	[["토코요A2"], ["恐", "恐怖", "A2トコヨ", "徒神トコヨ"]],
	[["오보로", "오보로O"], ["忍", "オボロ"]],
	[["오보로A"], ["戦", "戦略", "Aオボロ"]],
	[["유키히", "유키히O"], ["傘", "ユキヒ"]],
	[["유키히A"], ["社", "社交", "Aユキヒ"]],
	[["신라", "신라O"], ["書", "シンラ"]],
	[["신라A"], ["経", "経典", "Aシンラ"]],
	[["하가네", "하가네O"], ["鎚", "ハガネ"]],
	[["하가네A"], ["金", "金床", "Aハガネ"]],
	[["치카게", "치카게O"], ["毒", "チカゲ"]],
	[["치카게A"], ["絆", "Aチカゲ"]],
	[["쿠루루", "쿠루루O"], ["枢", "絡", "絡繰", "クルル"]],
	[["쿠루루A1", "쿠루루A"], ["機", "機器", "Aクルル", "A1クルル"]],
	[["쿠루루A2"], ["友", "友情", "徒神クルル"]],
	[["탈리야", "탈리야O"], ["騎", "乗", "乗騎", "サリヤ"]],
	[["탈리야A"], ["新", "新型", "Aサリヤ"]],
	[["라이라", "라이라O"], ["爪", "ライラ"]],
	[["라이라A"], ["嵐", "Aライラ"]],
	[["우츠로", "우츠로O"], ["鎌", "ウツロ"]],
	[["우츠로A"], ["塵", "Aウツロ"]],
	[["호노카", "호노카O"], ["旗", "ホノカ"]],
	[["호노카A", "오우카"], ["勾", "玉", "勾玉", "Aホノカ"]],
	[["코르누"], ["橇", "コルヌ"]],
	[["야츠하", "야츠하O"], ["鏡", "ヤツハ"]],
	[["야츠하A"], ["花", "Aヤツハ"]],
	[["야츠하AA"], ["魂", "AAヤツハ"]],
	[["하츠미", "하츠미O"], ["櫂", "ハツミ"]],
	[["하츠미A"], ["信", "信頼", "Aハツミ", "徒神ハツミ"]],
	[["미즈키"], ["兜", "ミズキ"]],
	[["메구미"], ["棹", "唐棹", "竿", "メグミ"]],
	[["카나에"], ["面", "仮面", "仮", "カナヱ"]],
	[["카무이"], ["剣", "カムヰ"]],
	[["렌리"], ["衣", "レンリ"]],
	[["아키나"], ["算", "算盤", "アキナ"]],
	[["시스이"], ["鋸", "シスイ"]],
];

function new_dict() {
	return { convert: Object.create(null), range: Object.create(null) };
}

function add_word(dict, from, to) {
	const ch = from.charAt(0);
	const r = dict.range[ch] ?? [to.length, to.length];
	dict.range[ch] = [Math.min(r[0], from.length), Math.max(r[1], from.length)];
	dict.convert[from] = to;
}

let dict_kr = new_dict();
let dict_jp = new_dict();

(function () {
	for (const krjp of WORDS) {
		const kr_list = krjp[0];
		const jp_list = krjp[1];
		for (const kr of kr_list) add_word(dict_kr, kr, jp_list[0]);
		for (const jp of jp_list) add_word(dict_jp, jp, kr_list[0]);
	}
})();

function translate(dict, s, opt = 0) {
	const MAGIC = "\ue652";
	let result = "";

	if (opt === 1) s = s.replaceAll(MAGIC, "");

	main: for (let i = 0; i < s.length; i++) {
		const r = dict.range[s.charAt(i)] ?? [1, 0];
		for (let j = r[1]; j >= r[0]; j--) {
			const c = dict.convert[s.substring(i, i + j).toUpperCase()];
			if (!c) continue;
			result += (opt === 1 ? MAGIC + c + MAGIC : c);
			i += j - 1;
			continue main;
		}
		result += s.charAt(i);
	}

	if (opt === 1) {
		result = result.replaceAll(MAGIC + MAGIC, " ").
			replaceAll(MAGIC + "(", " (").
			replaceAll(MAGIC, "");
	} else if (opt === 2) {
		result = result.replaceAll(" ", "");
	}
	return result;
}

function kr2jp() {
	let s = document.getElementById("txt_kr").value;
	s = translate(dict_kr, s, 2);
	document.getElementById("txt_jp").value = s;
	return true;
}

function jp2kr() {
	let s = document.getElementById("txt_jp").value;
	s = translate(dict_jp, s, 1);
	document.getElementById("txt_kr").value = s;
	return true;
}
