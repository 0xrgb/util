/** @type {import('tailwindcss').Config} */
module.exports = {
	relative: true,
	content: ["./public/**/*.html"],
	theme: {
		extend: {},
	},
	plugins: [
		require('@catppuccin/tailwindcss')({
			prefix: 'ctp',
		}),
	],
}
